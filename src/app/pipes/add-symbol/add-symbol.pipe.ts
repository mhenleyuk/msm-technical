import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addSymbol'
})
export class AddSymbolPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value = (Math.round(value * 100) / 100).toLocaleString();
    return `${args} ${value}`;
  }

}
