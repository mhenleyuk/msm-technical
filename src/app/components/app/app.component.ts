import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { TickerService } from '../../services/ticker/ticker.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
 @Input() model$: Observable<any>;

  constructor(protected service: TickerService) {}

  ngOnInit() {
    this.model$ = this.service.getItems();
  }


}
