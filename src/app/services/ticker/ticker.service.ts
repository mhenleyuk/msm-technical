import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { interval } from 'rxjs';
import { timer } from 'rxjs';

const BASE_API = 'https://blockchain.info';

@Injectable({
  providedIn: 'root'
})
export class TickerService {
  interval = 30000; // 30 Seconds

  constructor(protected http: HttpClient) {
   }

  getItems(): Observable<any> {

    return timer(0, this.interval).pipe(
      switchMap(() => this.http.get(`${BASE_API}/ticker`)),
      map((response) => {
        const currencies = Object.keys(response)
        .map((i) => {
          let currency = response[i];
          currency['key'] = i;
          return currency;
         });
        return currencies;
      })
    );
  }
}
