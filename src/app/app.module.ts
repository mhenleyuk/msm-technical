import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AddSymbolPipe } from './pipes/add-symbol/add-symbol.pipe';
import { AppComponent } from './components/app/app.component';

@NgModule({
  declarations: [
    AppComponent,
    AddSymbolPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
