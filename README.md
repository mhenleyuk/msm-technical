# MsmTechnical

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

## How to run

If you happen to want to run the application, clone the repo and then run 'npm run complete' in the terminal/cmd.
This should install all required dependencies and then start the node server on 'localhost:4200';